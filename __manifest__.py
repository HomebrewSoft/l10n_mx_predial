{
    "name": "Predial Node for Mexican EDI",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "l10n_mx_edi",
        "apelsa_custom",
    ],
    "data": [
        # security
        # data
        # reports
        "reports/cfdi.xml",
        "reports/invoice_report.xml",
        # views
        "views/account_move.xml",
        "views/product_template.xml",
    ],
}
