from odoo import _, api, fields, models


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    predial = fields.Char(
        string="Cuenta Predial",
    )
    enable_predial = fields.Boolean(related="product_id.enable_predial")
