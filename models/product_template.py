from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = "product.template"

    enable_predial = fields.Boolean(string="Habilitar Cuenta Predial")
